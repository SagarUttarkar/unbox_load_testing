package modules;

import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

public class Infl_tracking {

	
	@Test(priority=1)
public void Testcase_pageload_INfl_tracking_IG() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("Infl_tracking_IG_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("infltrack@test.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/infltracking.svg')]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//li[normalize-space()='Influencer Tracking List']")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='infl_ig_list no-padding']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("therock");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//i[@class='fa fa-eye view_right']")).click();
		Thread.sleep(5000);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
         Thread.sleep(5000);
		driver.findElement(By.id("link_for_IG1")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_IG3")).click();
		Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
						driver.close();
						driver.switchTo().window(tabs2.get(0));
						Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
						driver.close();

		
		Har har=myProxy.getHar();
		File Infl_track_ig_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/Infl_tracking_IG_tabs.har");
		har.writeTo(Infl_track_ig_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
	
	}
	@Test(priority=2)
public void Testcase_pageload_INfl_tracking_TW() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("Infl_tracking_TW_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("infltrack@test.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/infltracking.svg')]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//li[normalize-space()='Influencer Tracking List']")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='infl_ig_list no-padding']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("amitabh bachchan");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//i[@class='fa fa-eye view_right']")).click();
		Thread.sleep(5000);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
         driver.switchTo().window(tabs2.get(1));
         Thread.sleep(5000);
		driver.findElement(By.id("link_for_IG1")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_IG3")).click();
		Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
						driver.close();
						driver.switchTo().window(tabs2.get(0));
						Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
					

		
		Har har=myProxy.getHar();
		File Infl_track_TW_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/Infl_tracking_TW_tabs.har");
		har.writeTo(Infl_track_TW_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
	@Test(priority=3)
public void Testcase_pageload_INfl_tracking_YT() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("Infl_tracking_YT_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("infltrack@test.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/infltracking.svg')]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//li[normalize-space()='Influencer Tracking List']")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='infl_ig_list no-padding']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("Amit Bhadana");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//i[@class='fa fa-eye view_right']")).click();
		Thread.sleep(5000);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
         driver.switchTo().window(tabs2.get(1));
         Thread.sleep(5000);
		driver.findElement(By.id("link_for_YT2")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_IG3")).click();
		Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
						driver.close();
						driver.switchTo().window(tabs2.get(0));
						Thread.sleep(5000);
						driver.findElement(By.xpath("/html/body/div[3]/div[1]/div")).click();
						Thread.sleep(2000);
						driver.findElement(By.xpath("/html/body/div[3]/div[3]/div[4]/a")).click();
						

		
		Har har=myProxy.getHar();
		File Infl_track_YT_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/Infl_tracking_YT_tabs.har");
		har.writeTo(Infl_track_YT_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
	
}
