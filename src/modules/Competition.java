package modules;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

public class Competition {

	

	@Test(priority=1)
public void Testcase_pageload_competition_IG() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("competition_IG_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(1200, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/competition.svg')]")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-md-12 competition_track_list_data']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("International Client Upload");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//p[@class='compare-details-text col-md-12 no-padding text-grey ']")).click();
         Thread.sleep(5000);
		driver.findElement(By.id("link_for_117")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_119")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_152")).click();
		Thread.sleep(5000);
						

		
		Har har=myProxy.getHar();
		File Competition_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/competition_IG_tabs.har");
		har.writeTo(Competition_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
	
	@Test(priority=2)
public void Testcase_pageload_competition_FB() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("competition_FB_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/competition.svg')]")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-md-12 competition_track_list_data']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("Pharmascience Fb");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//p[@class='compare-details-text col-md-12 no-padding text-grey ']")).click();
         Thread.sleep(5000);
		driver.findElement(By.id("link_for_112")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_113")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_114")).click();
		Thread.sleep(5000);

		
		Har har=myProxy.getHar();
		File Competition_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/competition_FB_tabs.har");
		har.writeTo(Competition_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
	
	@Test(priority=3)
public void Testcase_pageload_competition_TW() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("competition_TW_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/competition.svg')]")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-md-12 competition_track_list_data']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("TW More Than 5");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//p[@class='compare-details-text col-md-12 no-padding text-grey ']")).click();
         Thread.sleep(5000);
         driver.findElement(By.id("link_for_147")).click();
 		Thread.sleep(5000);
 		driver.findElement(By.id("link_for_148")).click();
 		Thread.sleep(5000);
 		driver.findElement(By.id("link_for_149")).click();
 		Thread.sleep(5000);

					

		
		Har har=myProxy.getHar();
		File Competition_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/competition_YT_tabs.har");
		har.writeTo(Competition_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
	@Test(priority=3)
public void Testcase_pageload_competition_YT() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("competition_YT_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[contains(@src,'https://app.unboxsocial.com/public/images/sidebar-icons/competition.svg')]")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='col-md-12 competition_track_list_data']")));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("Top Indian Youtubers");
		Thread.sleep(5000);
		driver.findElement(By.xpath("//p[@class='compare-details-text col-md-12 no-padding text-grey ']")).click();
         Thread.sleep(5000);
 		driver.findElement(By.id("link_for_155")).click();
 		Thread.sleep(5000);
 		driver.findElement(By.id("link_for_156")).click();
 		Thread.sleep(5000);
 		driver.findElement(By.id("link_for_157")).click();
 		Thread.sleep(5000);
 		driver.findElement(By.id("link_for_158")).click();
 		Thread.sleep(5000);
					

		
		Har har=myProxy.getHar();
		File Competition_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/competition_YT_tabs.har");
		har.writeTo(Competition_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}	
}
