package modules;

import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

public class Analytics {

	
	
	@Test(priority=1)
	public void Testcase_pageload_Analytics_FB() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		// sagar testing
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("analytics_FB_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]")));
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("facebook");
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")));
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_85")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_84")).click();
		

		
		Thread.sleep(5000);
		
		Har har=myProxy.getHar();
		File AUD_FB_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/analytics_FB_tabs.har");
		har.writeTo(AUD_FB_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}
	@Test(priority=2)
public void Testcase_pageload_Analytics_IG() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("analytics_IG_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]")));
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("instagram");
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")));
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_29")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_31")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_32")).click();

		
		Thread.sleep(5000);
		
		Har har=myProxy.getHar();
		File AUD_FB_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/analytics_IG_tabs.har");
		har.writeTo(AUD_FB_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}
	@Test(priority=3)
public void Testcase_pageload_Analytics_TW() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("analytics_TW_tabs");
		
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);  
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]")));
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("twitter");
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")));
		driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]/div[2]/div[1]/div/div[3]/h6/a")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_127")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_128")).click();
		
		Thread.sleep(5000);
		
		Har har=myProxy.getHar();
		File AUD_FB_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/analytics_TW_tabs.har");
		har.writeTo(AUD_FB_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}
	
	@Test(priority=4)
public void Testcase_pageload_Analytics_YT() throws InterruptedException, IOException {
		
		//1. Start the proxy on some port
		BrowserMobProxy myProxy=new BrowserMobProxyServer();
		
		myProxy.start(0);
		
		//2. Set SSL and HTTP proxy in SeleniumProxy
		Proxy seleniumProxy=new Proxy();
		seleniumProxy.setHttpProxy("localhost:" +myProxy.getPort());
		seleniumProxy.setSslProxy("localhost:" +myProxy.getPort());
		
		//3. Add Capability for PROXY in DesiredCapabilities
		DesiredCapabilities capability=new DesiredCapabilities();
		capability.setCapability(CapabilityType.PROXY, seleniumProxy);
		capability.acceptInsecureCerts();
		capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		
		//4. Set captureTypes
		EnumSet <CaptureType> captureTypes=CaptureType.getAllContentCaptureTypes();
		captureTypes.addAll(CaptureType.getCookieCaptureTypes());
		captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
		captureTypes.addAll(CaptureType.getRequestCaptureTypes());
		captureTypes.addAll(CaptureType.getResponseCaptureTypes());
		
		//5. setHarCaptureTypes with above captureTypes
		myProxy.setHarCaptureTypes(captureTypes);
		
		//6. HAR name
		myProxy.newHar("analytics_YT_tabs");
		//7. Start browser and open URL
		
		System.setProperty("webdriver.chrome.driver","Drivers\\chromedriver_v95.exe");
		
		ChromeOptions options=new ChromeOptions();
		options.merge(capability);
		WebDriver driver=new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 1200);
		//Print Driver Capabilities
		System.out.println("Driver Capabilities===> \n" +((RemoteWebDriver)driver).getCapabilities().asMap().toString());
		driver.manage().window().maximize();
		// steps to be perform 
		
		driver.get("https://app.unboxsocial.com/");
		Thread.sleep(2000);
		driver.findElement(By.id("email")).sendKeys("sagar.uttarkar@unboxsocial.com");
		driver.findElement(By.id("pwd")).sendKeys("123456");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"login\"]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/ul[1]/li[3]/a[1]")).click();
		Thread.sleep(8000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/div/div/div[2]")));
		driver.findElement(By.xpath("//img[@class='list-search-icon search-anywhere-in-page']")).click();
		driver.findElement(By.xpath("//input[@name='serach-post']")).sendKeys("disney");
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'DisneyPlus Hotstar_IN')]")));
		driver.findElement(By.xpath("//div[contains(text(),'DisneyPlus Hotstar_IN')]")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_35")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("link_for_37")).click();
		
		Thread.sleep(5000);
		
		Har har=myProxy.getHar();
		File AUD_YT_myHARFile=new File(System.getProperty("user.dir")+"/HarFolder/analytics_YT_tabs.har");
		har.writeTo(AUD_YT_myHARFile);
		
		System.out.println("==> HAR details has been successfully written in the file.....");
	
		driver.close();
	}
}